return {
    ANNOUNCE_DEHUMID = {
        "The haze has lifted.",
    },
    ANNOUNCE_HAYFEVER = "Mine nostrils chafe with itchiness!",
    ANNOUNCE_HAYFEVER_OFF = "The smoke has cleared!",
    ANNOUNCE_SNEEZE = "aaAAAH...cho!",
    ANNOUNCE_TOO_HUMID = {
        "This %s is crushing my spirit.",
        "This %s burns like Sol.",
    },
    DESCRIBE = {
        ALLOY = "A metal worthy of the Sons of Ivaldi.",
        APORKALYPSE_CLOCK = "It controls Ragnarok!",
        ARMOR_METALPLATE = "Armor worthy of my deeds.",
        ARMOR_WEEVOLE = "Armor made from the skin of my enemy.",
        ASPARAGUS_PLANTED = "Useless vegetation!",
        BASEFAN = "Conquers the heat!",
        CHITIN = "The feeble armor of the bug.",
        CUTNETTLE = "Its smell has vanquish mine sinuses.",
        FLOWER_RAINFOREST = "A blossom most lovely.",
        GLOWFLY = {
            DEAD = "Alas, it is dead.",
            GENERIC = "Fly free, small friend.",
            SLEEPING = "A slumber consumes it.",
        },
        GLOWFLY_COCOON = "It has armored itself for sleep.",
        GRASS_TALL = {
            BURNING = "It burns!",
            GENERIC = "Surely it is the grass of Jotunheim!",
            PICKED = "I have defeated it!",
        },
        HALBERD = "Tis no spear, yet is still valorous!",
        IRON = "Material for metalwork.",
        PEAGAWK = {
            DEAD = "Even in death its eyes stare into my soul.",
            GENERIC = "It has the eyes of Odin.",
            SLEEPING = "I cannot tell if it is awake or sleep.",
        },
        PEAGAWKFEATHER = "A priceless beauty.",
        PEAGAWK_BUSH = "Do you think you can hide from me?",
        RABID_BEETLE = {
            DEAD = "It has died in battle.",
            GENERIC = "Evil beast. You have met your match!",
            SLEEPING = "Slumber consumes it!",
        },
        SHEARS = "A weapon for use against a mighty hedge!",
        SMELTER = {
            BURNT = "My role is more warrior than artizan.",
            COOKING_SHORT = "Patience is an essential quality of a good fighter.",
            DONE = "Now make me a sharp edge!",
            EMPTY = "The tools of dwarves.",
        },
        TREE_PILLAR = "Surely I stand before Yggdrasil!",
        WEEVOLE = "A pest upon this land!",
        WEEVOLE_CARAPACE = "The skin of my enemy.",
    },
}
